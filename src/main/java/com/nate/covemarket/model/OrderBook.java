package com.nate.covemarket.model;

import com.nate.covemarket.model.api.response.*;
import com.nate.covemarket.util.comparator.AskComparator;
import com.nate.covemarket.util.comparator.BidComparator;
import com.nate.covemarket.util.ui.GuiUpdateListener;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class OrderBook {

    private final Map<BigDecimal, Double> bids;
    private final Map<BigDecimal, Double> asks;
    private final GuiUpdateListener guiUpdateListener;

    private OrderBook(Map<BigDecimal, Double> bids, Map<BigDecimal, Double> asks, GuiUpdateListener guiUpdateListener) {
        this.bids = bids;
        this.asks = asks;
        this.guiUpdateListener = guiUpdateListener;
    }

    public static OrderBook create(GuiUpdateListener guiUpdateListener) {
        Map<BigDecimal, Double> bids = new TreeMap<>();
        Map<BigDecimal, Double> asks = new TreeMap<>();

        return new OrderBook(bids, asks, guiUpdateListener);
    }

    public void updateBook(LvlTwoUpdate update ) {
        Trade trade = update.getTrades().get(0);
        Trade.Side side  = trade.getSide();
        if (trade.getSize() == 0.0) {
            return;
        } else {
            if (side.equals(Trade.Side.BUY)) {
                bids.put(trade.getPrice(), trade.getSize());
            } else {
                asks.put(trade.getPrice(), trade.getSize());
            }
        }

        updateTopOfBook();
    }

    public void addSnapshot(Snapshot snapshot) {
        List<Bid> bids = snapshot.getBids();
        List<Ask> asks = snapshot.getAsks();

        for (Bid bid : bids) {
            this.bids.put(bid.getPrice(), bid.getSize());
        }

        for (Ask ask : asks) {
            this.asks.put(ask.getPrice(), ask.getSize());
        }
    }

    private void updateTopOfBook() {
        SortedSet<Map.Entry<BigDecimal, Double>> bids = sortBids();
        SortedSet<Map.Entry<BigDecimal, Double>> asks = sortAsks();

        List<Bid> bidList = bids.stream().limit(5).map(entry -> Bid.of(entry.getKey(), entry.getValue())
        ).collect(Collectors.toList());

        List<Ask> askList = asks.stream().limit(5).map(entry -> Ask.of(entry.getKey(), entry.getValue())
        ).collect(Collectors.toList());

        guiUpdateListener.onUpdate(bidList, askList, getMidpoint(bidList), getMidpoint(askList));
    }

    private double getMidpoint(List<? extends HasPriceAndSize> list) {
        double numerator = 0.0;
        double denominator = 0.0;

        if (list.size() > 0) {
            for (int i = 0; i < list.size() - 1; i++) {
                HasPriceAndSize element = list.get(i);
                double totalCost = element.getPrice().doubleValue() * element.getSize();
                numerator += totalCost;
                denominator += element.getSize();
            }
            return numerator / denominator;
        }
        return 0.0;
    }

    private SortedSet<Map.Entry<BigDecimal, Double>> sortBids() {
        return bids.entrySet().stream().collect(Collectors.toCollection(() -> new TreeSet<>(new BidComparator())));
    }

    private SortedSet<Map.Entry<BigDecimal, Double>> sortAsks() {
        return asks.entrySet().stream().collect(Collectors.toCollection(() -> new TreeSet<>(new AskComparator())));
    }
}

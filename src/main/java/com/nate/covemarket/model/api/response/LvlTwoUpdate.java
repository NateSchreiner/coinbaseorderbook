package com.nate.covemarket.model.api.response;

import lombok.Getter;

import java.time.OffsetDateTime;
import java.util.List;

public class LvlTwoUpdate {

    private static final String TYPE = "l2update";

    @Getter
    private final String productId;
    @Getter
    private final List<Trade> trades;
    @Getter
    private final OffsetDateTime timestamp;

    private LvlTwoUpdate(String productId, List<Trade> trades, OffsetDateTime timestamp) {
        this.productId = productId;
        this.trades = trades;
        this.timestamp = timestamp;
    }

    public static LvlTwoUpdate create(String productId, List<Trade> trades, OffsetDateTime timestamp) {
        return new LvlTwoUpdate(productId, trades, timestamp);
    }

    @Override
    public String toString() {
        Trade trade = trades.get(0);
        return "" + trade.getSize() + "     @       " + trade.getPrice();
    }
}


package com.nate.covemarket.model.api.response;

import java.math.BigDecimal;

public class Ask implements HasPriceAndSize {

    private final BigDecimal price;
    private final Double size;

    private Ask(BigDecimal price, double size) {
        this.price = price;
        this.size = size;
    }

    public static Ask of(String price, String size) {
        return new Ask(new BigDecimal(price), Double.valueOf(size));
    }

    public static Ask of(BigDecimal price, double size) {
        return new Ask(price, size);
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public double getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "" + size + "    @    " + price;
    }
}

package com.nate.covemarket.model.api.response;

import lombok.Getter;

public enum Type {
    snapshot("snapshot"),
    l2update("l2update"),
    error("error"),
    subscription("subscriptions");

    @Getter String value;

    Type(String value) {
        this.value = value;
    }

    public static Type getType(String value) {
        switch (value) {
            case "snapshot":
            return snapshot;
            case "l2update":
                return l2update;
            case "error":
                return error;
            case "subscriptions":
                return subscription;
            default:
                throw new IllegalStateException("Could not parse type: " + value);
        }
    }
}

package com.nate.covemarket.model.api.request;

public enum ChannelName {
    heartbeat, status, ticker, level2, user, matches, full;
}

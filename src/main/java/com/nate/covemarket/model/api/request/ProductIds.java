package com.nate.covemarket.model.api.request;

import lombok.Getter;

public enum ProductIds {
    BTC_USD("BTC-USD");

    @Getter
    String name;

    ProductIds(String name) {
        this.name = name;
    }
}

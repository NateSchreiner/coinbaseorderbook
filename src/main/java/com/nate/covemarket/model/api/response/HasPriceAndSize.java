package com.nate.covemarket.model.api.response;

import java.math.BigDecimal;

public interface HasPriceAndSize {

    BigDecimal getPrice();
    double getSize();
}

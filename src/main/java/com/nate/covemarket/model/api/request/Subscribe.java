package com.nate.covemarket.model.api.request;

import java.util.List;

public class Subscribe {

    private final String type = "subscribe";
    private final List<String> product_ids;
    private final List<Channel> channels;

    private Subscribe(List<String> product_ids, List<Channel> channels) {
        this.product_ids = product_ids;
        this.channels = channels;
    }

    public static Subscribe of(List<String> product_ids, List<Channel> channels) {
        return new Subscribe(product_ids, channels);
    }
}

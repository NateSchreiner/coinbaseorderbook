package com.nate.covemarket.model.api.response;

import lombok.Getter;

import java.math.BigDecimal;

public class Trade {

    @Getter private final Side side;
    @Getter private final BigDecimal price;
    @Getter private final double size;

    private Trade(Side side, BigDecimal price, double size) {
        this.side = side;
        this.price = price;
        this.size = size;
    }

    public static Trade create(Side side, BigDecimal price, double size) {
        return new Trade(side, price, size);
    }

    public enum Side {
        BUY("buy"),
        SELL("sell");

        String name;

        Side(String name) {
            this.name = name;
        }

        public static Side of(String value) {
            switch (value) {
                case "buy":
                    return BUY;
                case "sell":
                    return SELL;
                default:
                    throw new IllegalStateException("Can not decipher side: " + value);
            }
        }
    }

}


package com.nate.covemarket.model.api.request;


import lombok.Getter;

import java.util.List;

public class Channel {

    @Getter private ChannelName name;
    @Getter private List<String> product_ids;

    private Channel(ChannelName name, List<String> productIds) {
        this.name = name;
        this.product_ids = productIds;
    }

    public static Channel of(ChannelName name, List<String> productIds) {
        return new Channel(name, productIds);
    }


}

package com.nate.covemarket.model.api.response;

import lombok.Getter;

import java.util.List;

public class Snapshot {

    private final static String TYPE = "snapshot";

    private final String productId;
    @Getter private final List<Bid> bids;
    @Getter private final List<Ask> asks;

    private Snapshot(String productId, List<Bid> bids, List<Ask> asks) {
        this.productId = productId;
        this.bids = bids;
        this.asks = asks;
    }

    public static Snapshot of(String productId, List<Bid> bids, List<Ask> asks) {
        return new Snapshot(productId, bids, asks);
    }
}

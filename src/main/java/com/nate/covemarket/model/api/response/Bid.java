package com.nate.covemarket.model.api.response;

import java.math.BigDecimal;

public class Bid implements HasPriceAndSize {

    private final BigDecimal price;
    private final Double size;

    private Bid(BigDecimal price, double size) {
        this.price = price;
        this.size = size;
    }

    public static Bid of(String price, double size) {
        return new Bid(new BigDecimal(price), size);
    }

    public static Bid of(String price, String size) {
        return new Bid(new BigDecimal(price), Double.valueOf(size));
    }

    public static Bid of(BigDecimal price, double size) {
        return new Bid(price, size);
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public double getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "" + size + "    @    " + price;
    }
}

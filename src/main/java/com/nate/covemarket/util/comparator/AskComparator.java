package com.nate.covemarket.util.comparator;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Map;

public class AskComparator implements Comparator<Map.Entry<BigDecimal, Double>> {
    @Override
    public int compare(Map.Entry<BigDecimal, Double> o1, Map.Entry<BigDecimal, Double> o2) {
        int priceCmp = o1.getKey().compareTo(o2.getKey());
        if (priceCmp == 0) {
            return Double.compare(o2.getValue(), o1.getValue());
        }
        return priceCmp;
    }
}

package com.nate.covemarket.util;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nate.covemarket.model.api.response.Ask;
import com.nate.covemarket.model.api.response.Bid;
import com.nate.covemarket.model.api.response.LvlTwoUpdate;
import com.nate.covemarket.model.api.response.Snapshot;
import com.nate.covemarket.util.deserializers.GsonHelper;

import java.util.ArrayList;
import java.util.List;

public class CoinbaseUtility {

    private static List<Ask> parseAskData(JsonElement element) {
        java.lang.reflect.Type askListType = new TypeToken<ArrayList<Ask>>(){}.getType();
        List<Ask> asks = GsonHelper.getInstance().fromJson(element, askListType);
        return asks;
    }

    private static List<Bid> parseBidData(JsonElement element) {
        java.lang.reflect.Type bidListType = new TypeToken<ArrayList<Bid>>(){}.getType();
        List<Bid> bids = GsonHelper.getInstance().fromJson(element, bidListType);
        return bids;
    }

    public static Snapshot parseSnapshot(JsonObject obj) {
        List<Bid> bids = parseBidData(obj.get("bids"));
        List<Ask> asks = parseAskData(obj.get("asks"));
        String productId = obj.get("product_id").getAsString();
        return Snapshot.of(productId, bids, asks);
    }

    public static LvlTwoUpdate parseUpdate(JsonObject obj) {
        LvlTwoUpdate update = GsonHelper.getInstance().fromJson(obj, LvlTwoUpdate.class);
        return update;
    }
}

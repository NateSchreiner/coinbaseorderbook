package com.nate.covemarket.util.deserializers;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.nate.covemarket.model.api.response.LvlTwoUpdate;
import com.nate.covemarket.model.api.response.Trade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.crypto.dsig.spec.ExcC14NParameterSpec;
import java.lang.reflect.Type;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

public class L2UpdateDeserializer implements JsonDeserializer<LvlTwoUpdate> {

    private final Logger log = LoggerFactory.getLogger(L2UpdateDeserializer.class);

    @Override
    public LvlTwoUpdate deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        try {
            JsonObject obj = jsonElement.getAsJsonObject();
            String product_id = obj.get("product_id").getAsString();
            java.lang.reflect.Type t = new TypeToken<ArrayList<Trade>>(){}.getType();
            List<Trade> trades = GsonHelper.getInstance().fromJson(obj.get("changes"), t);
            OffsetDateTime time = OffsetDateTime.parse(obj.get("time").getAsString());
            return LvlTwoUpdate.create(product_id, trades, time);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }
}



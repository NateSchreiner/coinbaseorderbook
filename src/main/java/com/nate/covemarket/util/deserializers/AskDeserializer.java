package com.nate.covemarket.util.deserializers;

import com.google.gson.*;
import com.nate.covemarket.model.api.response.Ask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;

public class AskDeserializer implements JsonDeserializer<Ask> {

    private final Logger log = LoggerFactory.getLogger(AskDeserializer.class);

    @Override
    public Ask deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        try {
            if (jsonElement instanceof JsonObject) {
                return Ask.of(jsonElement.getAsJsonObject().get("price").getAsString(), jsonElement.getAsJsonObject().get("size").getAsString());
            } else {
                return Ask.of(((JsonArray)jsonElement).get(0).getAsString(), ((JsonArray)jsonElement).get(1).getAsString());
            }
        } catch (ClassCastException e) {
            log.error(e.getMessage());
            throw e;
        }
    }
}

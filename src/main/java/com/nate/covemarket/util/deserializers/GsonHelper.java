package com.nate.covemarket.util.deserializers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.nate.covemarket.model.api.response.Ask;
import com.nate.covemarket.model.api.response.Bid;
import com.nate.covemarket.model.api.response.LvlTwoUpdate;
import com.nate.covemarket.model.api.response.Trade;

public class GsonHelper {

    private static final Gson GSON;

    static {
        GsonBuilder builder = new GsonBuilder()
                .registerTypeAdapter(Ask.class, new AskDeserializer())
                .registerTypeAdapter(Bid.class, new BidDeserializer())
                .registerTypeAdapter(Trade.class, new TradeDeserializer())
                .registerTypeAdapter(LvlTwoUpdate.class, new L2UpdateDeserializer());
        GSON = builder.create();
    }

    public static Gson getInstance() {
        return GSON;
    }

    public static String toJson(Object obj, Class c) {
        return GSON.toJson(obj, c);
    }

    public static JsonObject fromJson(String json) {
        return GSON.fromJson(json, JsonObject.class);
    }

}

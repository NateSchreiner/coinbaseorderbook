package com.nate.covemarket.util.deserializers;

import com.google.gson.*;
import com.nate.covemarket.model.api.response.Trade;

import java.lang.reflect.Type;

public class TradeDeserializer implements JsonDeserializer<Trade> {
    @Override
    public Trade deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonArray arr = ((JsonArray)jsonElement);
        Trade trade = Trade.create(Trade.Side.of(arr.get(0).getAsString()), arr.get(1).getAsBigDecimal(), arr.get(2).getAsDouble());
        return trade;
    }
}

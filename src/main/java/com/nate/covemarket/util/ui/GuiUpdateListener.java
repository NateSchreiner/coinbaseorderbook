package com.nate.covemarket.util.ui;

import com.nate.covemarket.model.api.response.Ask;
import com.nate.covemarket.model.api.response.Bid;

import java.util.List;

public interface GuiUpdateListener {

    void onUpdate(List<Bid> bids, List<Ask> asks, double bidMid, double askMid);
}

package com.nate.covemarket.util.ui;

import com.nate.covemarket.model.api.request.ProductIds;
import com.nate.covemarket.model.api.response.Ask;
import com.nate.covemarket.model.api.response.Bid;

import java.text.DecimalFormat;
import java.util.List;


public class ConsoleUpdater implements GuiUpdateListener {

    private final DecimalFormat sizeDf;
    private final DecimalFormat priceDf;

    private ConsoleUpdater(DecimalFormat sizeDf, DecimalFormat priceDf) {
        this.sizeDf = sizeDf;
        this.priceDf = priceDf;
    }

    public static ConsoleUpdater create(String sizeDecimalFormat, String priceDecimalFormat) {
        DecimalFormat sizeDf = new DecimalFormat(sizeDecimalFormat);
        DecimalFormat priceDf = new DecimalFormat(priceDecimalFormat);

        return new ConsoleUpdater(sizeDf, priceDf);
    }

    @Override
    public void onUpdate(List<Bid> bids, List<Ask> asks, double bidMid, double askMid) {
        System.out.println("  ");
        System.out.println("  ");
        System.out.println("  ");

        System.out.println("Product: " + ProductIds.BTC_USD.getName());
        System.out.println("Average Prices:    Bid: " + priceDf.format(bidMid) + "    Ask: " + priceDf.format(askMid));
        System.out.print("-------- Bids --------");
        System.out.print("           ");
        System.out.println("-------- Asks --------");

        for (int i = 0; i < Math.min(bids.size(), asks.size()); i++) {
            System.out.print("" + sizeDf.format(bids.get(i).getSize()) + "  @   $" + priceDf.format(bids.get(i).getPrice()) + "      ");
            System.out.println("" + sizeDf.format(asks.get(i).getSize()) + "  @  $" + priceDf.format(asks.get(i).getPrice()) + "      ");
        }
    }
}

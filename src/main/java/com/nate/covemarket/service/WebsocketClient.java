package com.nate.covemarket.service;

import com.google.gson.JsonObject;
import com.nate.covemarket.model.api.request.Channel;
import com.nate.covemarket.model.api.request.ChannelName;
import com.nate.covemarket.model.api.request.ProductIds;
import com.nate.covemarket.model.api.request.Subscribe;
import com.nate.covemarket.model.api.response.*;
import com.nate.covemarket.model.OrderBook;
import com.nate.covemarket.util.CoinbaseUtility;
import com.nate.covemarket.util.deserializers.GsonHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.client.WebSocketClient;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.nate.covemarket.util.deserializers.GsonHelper.toJson;

@ClientEndpoint
public class WebsocketClient {

    private static final Logger log = LoggerFactory.getLogger(WebSocketClient.class);

    private final URI uri;
    private final OrderBook orderBook ;

    private Session userSession;

    private WebsocketClient(URI uri, OrderBook orderBook) {
        this.uri = uri;
        this.orderBook = orderBook;
    }

    public static WebsocketClient create(String uri, OrderBook orderBook) {
        URI u;
        try {
            u = new URI(uri);
        } catch (URISyntaxException e) {
            log.error(e.getMessage());
            throw new IllegalStateException("no URI");
        }
        return new WebsocketClient(u, orderBook);
    }

    public void connectToServer() {
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.setDefaultMaxBinaryMessageBufferSize(1024*1024);
            container.setDefaultMaxTextMessageBufferSize(1024*1024);
            container.connectToServer(this, uri);
        } catch (DeploymentException | IOException e) {
            log.error(e.getMessage());
        }
    }

    @OnOpen
    public void onOpen(Session session) {
        this.userSession = session;
        log.debug("Connected ... " + session.getId());

        Subscribe msg = Subscribe.of(
                Collections.singletonList(ProductIds.BTC_USD.getName()),
                Stream.of(
                        Channel.of(ChannelName.level2, Collections.singletonList(ProductIds.BTC_USD.getName()))
                ).collect(Collectors.toList())
        );

        session.getAsyncRemote().sendText(toJson(msg, Subscribe.class));
    }

    @OnMessage
    public String onMessage(String message, Session session) {

        JsonObject jsonObj =  GsonHelper.fromJson(message);
        Type type = Type.getType(jsonObj.get("type").getAsString());

        if (type.equals(Type.l2update)) {
            LvlTwoUpdate update = CoinbaseUtility.parseUpdate(jsonObj);
            orderBook.updateBook(update);
        } else if (type.equals(Type.snapshot)) {
            Snapshot snap = CoinbaseUtility.parseSnapshot(jsonObj);
            orderBook.addSnapshot(snap);
        } else if (type.equals(Type.error)){
            log.debug("Coinbase sent error ... " + jsonObj.get("error"));
        }
        return message;
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        log.debug(String.format("Session %s close because of %s", session.getId(), closeReason));
    }
}
